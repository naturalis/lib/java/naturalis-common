/**
 * Basic I/O utilities.
 *
 * @author Ayco Holleman
 */
package nl.naturalis.common.io;
