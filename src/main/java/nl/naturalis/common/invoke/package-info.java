/**
 * A set of classes that uses the {@code java.lang.invoke} package rather than reflection to
 * implement bean manipulation functionality.
 *
 * @author Ayco Holleman
 */
package nl.naturalis.common.invoke;
