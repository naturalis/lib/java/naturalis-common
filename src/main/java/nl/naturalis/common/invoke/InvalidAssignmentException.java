package nl.naturalis.common.invoke;

import static nl.naturalis.common.ClassMethods.simpleClassName;

import java.util.Collection;
import java.util.Map;
import nl.naturalis.common.TypeConversionException;

public final class InvalidAssignmentException extends InvokeException {

  private final String propertyName;
  private final Class<?> propertyType;
  private final Object value;

  InvalidAssignmentException(Setter setter, TypeConversionException e) {
    super(
        "Cannot assign value %s to field \"%s\": %s",
        valToString(e.getObjectToConvert()), setter.getProperty(), e.getMessage());
    this.propertyName = setter.getProperty();
    this.propertyType = setter.getParamType();
    this.value = e.getObjectToConvert();
  }

  public String getPropertyName() {
    return propertyName;
  }

  public Class<?> getPropertyType() {
    return propertyType;
  }

  public Object getValue() {
    return value;
  }

  private static String valToString(Object val) {
    if (val == null) {
      return "null";
    } else if (val instanceof CharSequence) {
      return "\"" + val + "\"";
    } else if (val instanceof Collection || val instanceof Map || val.getClass().isArray()) {
      return simpleClassName(val);
    }
    return val.toString();
  }
}
