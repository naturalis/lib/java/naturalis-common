# naturalis-common

Naturalis Java Language Extensions Library

Java module containing basic language extensions and utility classes.

- Small footprint
- Self-contained  (zero dependencies outside the `java.*` namespace)
- Avoid duplication of functionality that is already present in libraries such as Apache Commons and Google Guava.
  However, since it is also meant to be self-contained, some overlap is inevitable

The javadocs can be
found [here](https://naturalis.gitlab.io/lib/java/naturalis-common/nl.naturalis.common/module-summary.html)



## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`



